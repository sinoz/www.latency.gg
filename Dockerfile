FROM node:16-alpine AS builder

# Consumes the NPM_VERSION argument from the command-line.
ARG NPM_VERSION

# Change the work directory within our image.
WORKDIR /root

# Copy over all necessary files into our image.
COPY src /root/src
COPY package.json package-lock.json tsconfig.json /root/

RUN npm version "${NPM_VERSION}"
RUN npm install --unsafe-perm

# Now that we're done building, let's start over with a clean image and
# copy over the binaries that we've produced during the building process.
FROM node:16-alpine

COPY --from=builder /root/package.json /root/package.json
COPY --from=builder /root/out /root/out
COPY --from=builder /root/node_modules /root/node_modules
COPY assets /root/assets

WORKDIR /root

ENTRYPOINT [ "node", "--unhandled-rejections", "strict", "--require", "source-map-support/register", "out/program" ]
