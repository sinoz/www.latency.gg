import { createServices } from "./service.js";
import { Settings } from "./settings.js";

export interface Context {
    config: Settings;
    destroy: () => Promise<void>;
}

export function createContext(
    config: Settings,
): Context {
    const services = createServices(config);

    const destroy = async () => {
        //
    };

    return {
        config,
        destroy,
    };
}
