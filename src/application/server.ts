import Koa from "koa";
import koaStatic from "koa-static";
import * as path from "path";
import { projectRoot } from "../utils/index.js";
import { Context } from "./context.js";

export function createServer(
    context: Context,
) {
    const server = new Koa();

    server.use(koaStatic(
        path.join(projectRoot, "assets"),
    ));

    return server;
}
