import { isAbortError, waitForAbort } from "abort-tools";
import { program } from "commander";
import delay from "delay";
import * as http from "http";
import { second } from "msecs";
import { WaitGroup } from "useful-wait-group";
import * as application from "../application/index.js";

program.
    command("server").
    option("--port <number>", "Listen to port", Number, 8080).
    option(
        "--self-endpoint <url>",
        "endpoint of the service",
        v => new URL(v),
        new URL("http://0.0.0.0"),
    ).
    option("--shutdown-delay <msec>", "", Number, 35 * second).
    action(programAction);

interface ActionOptions {
    port: number;
    selfEndpoint: URL;
    shutdownDelay: number;
}
async function programAction(actionOptions: ActionOptions) {
    const {
        port,
        selfEndpoint,
        shutdownDelay,
    } = actionOptions;

    console.log("starting...");

    process.
        on("uncaughtException", error => {
            console.error(error);
            process.exit(1);
        }).
        on("unhandledRejection", error => {
            console.error(error);
            process.exit(1);
        });

    const onWarn = (error: unknown) => {
        console.warn(error);
    };

    const livenessController = new AbortController();
    const readinessController = new AbortController();

    {
        /*
        When we receive a signal, the server should be not ready anymore, so
        we abort the readiness controller
        */
        const onSignal = (signal: NodeJS.Signals) => {
            console.log(signal);
            readinessController.abort();
        };
        process.addListener("SIGINT", onSignal);
        process.addListener("SIGTERM", onSignal);
    }

    const config: application.Settings = {
        selfEndpoint,
    };

    const applicationContext = application.createContext(config);

    /*
    We use the waitgroup to wait for requests to finish
    */
    const waitGroup = new WaitGroup();

    const applicationServer = application.createServer(
        applicationContext,
    );

    applicationServer.use(
        (context, next) => waitGroup.with(next),
    );

    const applicationHttpServer = http.createServer(applicationServer.callback());

    await new Promise<void>(resolve => applicationHttpServer.listen(
        port,
        () => resolve(),
    ));

    console.log("started");

    try {
        await waitForAbort(readinessController.signal, "application aborted");
    }
    catch (error) {
        if (!isAbortError(error)) throw error;
    }

    console.log("stopping...");

    /*
    wait a bit, usually for the load balancer to recognize this service as
    not ready and move traffic to another service.
    TODO: implement heartbeat so this will actually work!
    */
    await delay(shutdownDelay);

    /*
    wait for all requests to finish
    */
    await waitGroup.wait();

    /*
    abort the liveness controller to terminate all streaming responses!
    */
    livenessController.abort();

    /*
    stop the server
    */
    await new Promise<void>((resolve, reject) => applicationHttpServer.close(
        error => error ?
            reject(error) :
            resolve(),
    ));

    await applicationContext.destroy();

    console.log("stopped");
}
