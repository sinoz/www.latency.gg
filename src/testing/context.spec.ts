import * as selenium from "selenium-webdriver";
import test from "tape-promise/tape.js";
import { withContext } from "../testing/index.js";

if (process.env.BROWSER) test("frontend client", t => withContext(async context => {
    const frontendClient = await context.createBrowser();
    await frontendClient.get(new URL("/", context.endpoints.frontend).toString());

    const elements = await frontendClient.findElements(selenium.By.css("img.logo"));
    t.equal(elements.length, 1);
}));
